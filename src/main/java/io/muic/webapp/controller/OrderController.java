package io.muic.webapp.controller;

import io.muic.webapp.api.entity.Orders;
import io.muic.webapp.api.form.Checkout;
import io.muic.webapp.api.form.CheckoutItem;
import io.muic.webapp.api.form.OrderResponse;
import io.muic.webapp.api.form.TotalStatus;
import io.muic.webapp.api.service.OrderDetailService;
import io.muic.webapp.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import java.util.List;

/**
 * Created by Bank on 11/5/17.
 */
@CrossOrigin(origins = "http://localhost", maxAge = 3600)
@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderDetailService orderDetailService;

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public List<Orders> getAllOrders() {
        return orderService.getAllOrder();
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
    public OrderResponse getOrderById(@PathVariable long id) {
        return orderService.customResponseById(id);
    }

    @RequestMapping(value = "/order/customer/{customerId}", method = RequestMethod.GET)
    public List<Orders> getOrderByCustomerId(@PathVariable long customerId) {
        return orderService.findByCustomerId(customerId);
    }

    @RequestMapping(value = "/order/total/status", method = RequestMethod.POST)
    public TotalStatus getOrderTotalStatus(@RequestParam String option, @RequestParam long id) {
        return orderService.getOrderTotalStatus(option, id);
    }

    @RequestMapping(value = "/order/update/status", method = RequestMethod.POST)
    public OrderResponse updateOrderStatus(@RequestParam long id, @RequestParam String status) {
        return orderService.updateStatusById(id, status);
    }

    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    public String checkout(@RequestBody Checkout checkout) throws ServletException {
        List<CheckoutItem> checkoutItems = checkout.getItems();
        String checked = orderDetailService.checkStock(checkoutItems);
        if (checked.equals("OK")){
            Orders order = orderService.create(
                    checkout.getStatus(), checkout.getName(), checkout.getAddress(),
                    checkout.getPostcode(), checkout.getDistrict(), checkout.getProvince(),
                    checkout.getPhoneNumber(), checkout.getTotal(), checkout.getCustomerId()
            );
            orderDetailService.createOrderDetail(checkoutItems, order.getOrderId());
            return "Your Order ID: "+order.getOrderId();
        } else {
            throw new ServletException(checked);
        }
    }

}
