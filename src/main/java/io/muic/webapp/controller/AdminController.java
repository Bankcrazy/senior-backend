package io.muic.webapp.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.muic.webapp.api.entity.Admin;
import io.muic.webapp.api.form.Credential;
import io.muic.webapp.api.service.AdminService;
import io.muic.webapp.configuration.Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import java.util.Date;

/**
 * Created by Bank on 10/30/17.
 */
@CrossOrigin(origins = "http://localhost", maxAge = 3600)
@RestController
public class AdminController {
    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/admin/add", method = RequestMethod.POST)
    public String registerUser(@RequestBody Admin add) throws ServletException {
        Admin admin = adminService.findByUsername(add.getUsername());
        if (admin == null){
            adminService.save(add);
            return "Register Successful";
        }

        throw new ServletException("Admin already exists");
    }

    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public Credential login(@RequestBody Admin login) throws ServletException {

        String jwtToken = "";

        if (login.getUsername() == null || login.getPassword() == null) {
            throw new ServletException("Please fill in username and password");
        }

        String username = login.getUsername();
        String password = login.getPassword();

        Admin admin = adminService.findByUsername(username);

        if (admin == null) {
            throw new ServletException("Invalid username or password");
        }

        String pwd = admin.getPassword();

        if (!Encryption.getSecurePassword(username,password).equals(admin.getPassword())) {
            throw new ServletException("Invalid username or password");
        }

        jwtToken = Jwts.builder().setSubject(username).claim("roles", "admin").setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

        Credential credit = new Credential();
        credit.setId(admin.getAdminId());
        credit.setUser(admin.getUsername());
        credit.setJwt(jwtToken);

        return credit;
    }
}
