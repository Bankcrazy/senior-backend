package io.muic.webapp.controller;

import io.muic.webapp.api.entity.Quotation;
import io.muic.webapp.api.form.OfferPrice;
import io.muic.webapp.api.form.QuotationResponse;
import io.muic.webapp.api.form.RequestQuotation;
import io.muic.webapp.api.form.TotalStatus;
import io.muic.webapp.api.service.QuotationDetailService;
import io.muic.webapp.api.service.QuotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import java.util.List;

/**
 * Created by Bank on 11/12/17.
 */
@CrossOrigin(origins = "http://localhost", maxAge = 3600)
@RestController
public class QuotationController {
    @Autowired
    private QuotationService quoteService;

    @Autowired
    private QuotationDetailService quoteDetailService;

    @RequestMapping(value = "/quotation", method = RequestMethod.GET)
    public List<Quotation> getAllQuotation() {
        return quoteService.getAllQuotation();
    }

    @RequestMapping(value = "/quotation/{id}", method = RequestMethod.GET)
    public QuotationResponse getQuotationById(@PathVariable long id) {
        return quoteService.customResponseById(id);
    }

    @RequestMapping(value = "/quotation/customer/{customerId}", method = RequestMethod.GET)
    public List<Quotation> getQuotationByCustomerId(@PathVariable long customerId) {
        return quoteService.findByCustomerId(customerId);
    }

    @RequestMapping(value = "/quotation/total/status", method = RequestMethod.POST)
    public TotalStatus getQuotationTotalStatus(@RequestParam String option, @RequestParam long id) {
        return quoteService.getQuotationTotalStatus(option, id);
    }

    @RequestMapping(value = "/quotation/update/status", method = RequestMethod.POST)
    public QuotationResponse updateQuotationStatus(@RequestParam long id, @RequestParam String status) {
        return quoteService.updateStatusById(id, status);
    }

    @RequestMapping(value = "/quotation/update/offerPrice", method = RequestMethod.POST)
    public QuotationResponse updateQuotationOfferPrice(@RequestBody OfferPrice offerPrice) {
        long quoteId = offerPrice.getQuotationId();
        quoteDetailService.updateOfferPrice(offerPrice.getItems());
        quoteService.updateStatusById(quoteId, "Offered");
        return quoteService.customResponseById(quoteId);
    }

    @RequestMapping(value = "/quotation/add", method = RequestMethod.POST)
    public String addProduct(@RequestBody RequestQuotation requestQuote) throws ServletException {
        Quotation quote = quoteService.create(
                requestQuote.getCompanyName(), requestQuote.getCompanyBranch(),
                requestQuote.getAddress(), requestQuote.getPostcode(),
                requestQuote.getDistrict(), requestQuote.getProvince(),
                requestQuote.getTaxId(), requestQuote.getContactName(),
                requestQuote.getContactTel(), requestQuote.getContactEmail(),
                requestQuote.getRemark(), requestQuote.getStatus(),
                requestQuote.getCustomerId(), requestQuote.getTotalItem(),
                requestQuote.getTotalQuantity()

        );
        quoteDetailService.createQuotationDetail(requestQuote.getItems(), quote.getQuotationId());
        return "Your Quotation ID: "+quote.getQuotationId();
    }
}
