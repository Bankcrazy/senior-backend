package io.muic.webapp.controller;

import io.muic.webapp.api.entity.Product;
import io.muic.webapp.api.form.ProductItem;
import io.muic.webapp.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import java.util.List;

/**
 * Created by Bank on 10/31/17.
 */
@CrossOrigin(origins = "http://localhost", maxAge = 3600)
@RestController
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public List<Product> getAllProduct() {
        return productService.getAllProduct();
    }

    @RequestMapping(value = "/product/selectList", method = RequestMethod.GET)
    public List<ProductItem> getAllProductSelectList() {
        return productService.getAllProductSelectList();
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public Product getProductById(@PathVariable long id) {
        return productService.findById(id);
    }

    // secure
    @RequestMapping(value = "/secure/product/add", method = RequestMethod.POST)
    public String addProduct(@RequestParam String pn, @RequestParam String pd,
                             @RequestParam int price, @RequestParam int quantity,
                             @RequestParam double length, @RequestParam double width,
                             @RequestParam double height, @RequestParam double weight,
                             @RequestParam MultipartFile file) throws ServletException {
        if (!file.isEmpty()) {
            try {
                byte[] image = file.getBytes();
                Product product = productService.create(pn, pd, price, quantity, length, width, height, weight, image);
                return "Product ID: "+product.getProductId();
            } catch (Exception e) {
                throw new ServletException("Failed to upload image");
            }
        }
        byte[] empty = new byte[0];
        Product product = productService.create(pn, pd, price, quantity, length, width, height, weight, empty);
        return "Product ID: "+product.getProductId();
    }

}
