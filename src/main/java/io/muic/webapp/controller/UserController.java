package io.muic.webapp.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.muic.webapp.api.entity.User;
import io.muic.webapp.api.form.Credential;
import io.muic.webapp.api.service.UserService;
import io.muic.webapp.configuration.Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import java.util.Date;
import java.util.List;

/**
 * Created by Bank on 10/18/17.
 */
@CrossOrigin(origins = "http://localhost", maxAge = 3600)
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<User> getAllUser() {
        return userService.getAllUser();
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User getUserById(@PathVariable long id) {
        return userService.findById(id);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUser(@RequestBody User register) throws ServletException {
        User user = userService.findByEmail(register.getEmail());
        if (user == null){
            if (!(register.getPassword().equals(register.getPasswordConfirmation()))){
                throw new ServletException("Password does not match");
            }
            userService.save(register);
            return "Register Successful";
        }

        throw new ServletException("Email already exists");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Credential login(@RequestBody User login) throws ServletException {

        String jwtToken = "";

        if (login.getEmail() == null || login.getPassword() == null) {
            throw new ServletException("Please fill in email and password");
        }

        String email = login.getEmail();
        String password = login.getPassword();

        User user = userService.findByEmail(email);

        if (user == null) {
            throw new ServletException("Invalid email or password");
        }

        if (!Encryption.getSecurePassword(email,password).equals(user.getPassword())) {
            throw new ServletException("Invalid email or password");
        }

        jwtToken = Jwts.builder().setSubject(email).claim("roles", "user").setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

        Credential credit = new Credential();
        credit.setId(user.getUserId());
        credit.setUser(user.getEmail());
        credit.setJwt(jwtToken);

        return credit;
    }
}
