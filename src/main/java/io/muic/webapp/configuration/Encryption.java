package io.muic.webapp.configuration;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Bank on 11/28/17.
 */
public class Encryption {
    public static String getSecurePassword(String username, String password)
    {
        String SALT = username + password;
        StringBuilder hash = new StringBuilder();
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha.digest(SALT.getBytes());
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f' };
            for (byte b : hashedBytes) {
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException ignored) {

        }
        return hash.toString();
    }
}
