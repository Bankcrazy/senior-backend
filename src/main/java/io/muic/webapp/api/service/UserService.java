package io.muic.webapp.api.service;

import io.muic.webapp.api.entity.User;
import io.muic.webapp.api.repository.UserRepository;
import io.muic.webapp.configuration.Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Bank on 10/18/17.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;

    public User findByEmail(String email) {
        return userRepo.findByEmail(email);
    }

    public User findById(long id) {
        return userRepo.findByUserId(id);
    }

    public User save(User input) {

        User user = new User();
        user.setFirstName(input.getFirstName());
        user.setLastName(input.getLastName());
        user.setEmail(input.getEmail());
        user.setPassword(Encryption.getSecurePassword(input.getEmail(),input.getPassword()));
        return userRepo.save(user);
    }

    public List<User> getAllUser() {
        return userRepo.findAll();
    }
}