package io.muic.webapp.api.service;

import io.muic.webapp.api.entity.Product;
import io.muic.webapp.api.entity.Quotation;
import io.muic.webapp.api.entity.QuotationDetail;
import io.muic.webapp.api.form.QuotationItem;
import io.muic.webapp.api.form.RequestQuotationItem;
import io.muic.webapp.api.repository.ProductRepository;
import io.muic.webapp.api.repository.QuotationDetailRepository;
import io.muic.webapp.api.repository.QuotationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Bank on 11/20/17.
 */
@Service
public class QuotationDetailService {
    @Autowired
    private QuotationRepository quoteRepo;
    @Autowired
    private QuotationDetailRepository quoteDetailRepo;
    @Autowired
    private ProductRepository productRepo;

    public QuotationDetail save(QuotationDetail quoteDetail) {
        return quoteDetailRepo.save(quoteDetail);
    }

    public String createQuotationDetail(List<RequestQuotationItem> requestQuotationItems, long quoteId){
        Quotation quote = quoteRepo.findByQuotationId(quoteId);
        for (final RequestQuotationItem requestQuotationItem : requestQuotationItems){
            long pId = requestQuotationItem.getpId();
            QuotationDetail qd = new QuotationDetail();
            Product product = productRepo.findByProductId(pId);

            qd.setProposedQuantity(requestQuotationItem.getQuantity());
            qd.setProduct(product);
            qd.setQuotation(quote);
            save(qd);
        }
        return "DONE";
    }

    public String updateOfferPrice(List<QuotationItem> quoteItems){
        for (QuotationItem item: quoteItems){
            QuotationDetail quoteDetail = quoteDetailRepo.findOne(item.getQuotationDetailId());
            quoteDetail.setOfferPrice(item.getOfferedPrice());
            save(quoteDetail);
        }
        return "OK";
    }
}
