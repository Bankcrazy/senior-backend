package io.muic.webapp.api.service;


import io.muic.webapp.api.entity.OrderDetail;
import io.muic.webapp.api.entity.Orders;
import io.muic.webapp.api.entity.Product;
import io.muic.webapp.api.form.CheckoutItem;
import io.muic.webapp.api.repository.OrderDetailRepository;
import io.muic.webapp.api.repository.OrderRepository;
import io.muic.webapp.api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Bank on 11/9/17.
 */
@Service
public class OrderDetailService {
    @Autowired
    private OrderDetailRepository orderDetailRepo;
    @Autowired
    private ProductRepository productRepo;
    @Autowired
    private OrderRepository orderRepo;

    public OrderDetail save(OrderDetail orderDetail) {
        return orderDetailRepo.save(orderDetail);
    }

    public List<OrderDetail> findByOrderId(long orderId){
        Orders order = orderRepo.findByOrderId(orderId);
        return orderDetailRepo.findByOrders(order);
    }

    public String checkStock(List<CheckoutItem> checkoutItems){
        for (CheckoutItem checkoutItem : checkoutItems){
            Product product = productRepo.findByProductId(checkoutItem.getProductId());
            if (checkoutItem.getAmount() > product.getQuantity()){
                return "Product " + product.getProductName() + " is out of stock";
            }
        }
        return "OK";
    }

    public String createOrderDetail(List<CheckoutItem> checkoutItems, long orderId){
        Orders order = orderRepo.findByOrderId(orderId);
        for (final CheckoutItem checkoutItem : checkoutItems){
            OrderDetail od = new OrderDetail();
            Product product = productRepo.findByProductId(checkoutItem.getProductId());

            int amount = checkoutItem.getAmount();
            int total = product.getPrice() * amount;

            product.setQuantity(product.getQuantity()-amount);

            od.setAmount(amount);
            od.setTotal(total);
            od.setProduct(product);
            od.setOrders(order);
            save(od);
        }
        return "DONE";
    }
}
