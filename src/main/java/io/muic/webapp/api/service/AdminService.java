package io.muic.webapp.api.service;

import io.muic.webapp.api.entity.Admin;
import io.muic.webapp.api.repository.AdminRepository;
import io.muic.webapp.configuration.Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Bank on 10/30/17.
 */
@Service
public class AdminService {
    @Autowired
    private AdminRepository adminRepo;


    public Admin findByUsername(String username) {
        return adminRepo.findByUsername(username);
    }

    public Admin save(Admin input) {
        Admin admin = new Admin();
        admin.setUsername(input.getUsername());
        admin.setPassword(Encryption.getSecurePassword(input.getUsername(),input.getPassword()));
        return adminRepo.save(admin);
    }

}
