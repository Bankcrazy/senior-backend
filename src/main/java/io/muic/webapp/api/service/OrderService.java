package io.muic.webapp.api.service;

import io.muic.webapp.api.entity.*;
import io.muic.webapp.api.form.OrderItem;
import io.muic.webapp.api.form.OrderResponse;
import io.muic.webapp.api.form.TotalStatus;
import io.muic.webapp.api.repository.OrderDetailRepository;
import io.muic.webapp.api.repository.OrderRepository;
import io.muic.webapp.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Order;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bank on 11/5/17.
 */
@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private OrderDetailRepository orderDetailRepo;

    public Orders save(Orders order) {
        return orderRepo.save(order);
    }

    public Orders findById(long id){
        return orderRepo.findByOrderId(id);
    }

    public List<Orders> getAllOrder() {
        return orderRepo.findAll();
    }

    public TotalStatus getOrderTotalStatus(String option, long id){

        List<Orders> orders = new ArrayList<>();
        if (option.equals("USER")){
            User user = userRepo.findByUserId(id);
            orders = orderRepo.findByUser(user);

        } else if (option.equals("ADMIN")){
            orders= getAllOrder();
        }

        TotalStatus response = new TotalStatus();
        int total = 0; int statA = 0; int statB = 0; int statC = 0; int statD = 0;
        for (Orders order:orders){
            String status = order.getStatus();
            total += 1;
            switch (status) {
                case "Placed":
                    statA += 1;
                    break;
                case "In-Progress":
                    statB += 1;
                    break;
                case "Shipped":
                    statC += 1;
                    break;
                case "Delivered":
                    statD += 1;
                    break;
            }
        }
        response.setTotal(total);
        response.setStatA(statA);
        response.setStatB(statB);
        response.setStatC(statC);
        response.setStatD(statD);
        return response;
    }

    public OrderResponse updateStatusById(long id, String status){
        Orders order = orderRepo.findByOrderId(id);
        order.setStatus(status);
        save(order);
        return customResponseById(id);
    }

    public List<Orders> findByCustomerId(long id) {
        User user = userRepo.findByUserId(id);
        return orderRepo.findByUser(user);
    }

    public OrderResponse customResponseById(long id) {
        Orders order = orderRepo.findByOrderId(id);
        List<OrderDetail> orderDetails = orderDetailRepo.findByOrders(order);
        OrderResponse response = new OrderResponse();
        response.setOrder(order);
        List<OrderItem> orderItems = new ArrayList<OrderItem>() {};
        for(OrderDetail detail: orderDetails){
            OrderItem orderItem = new OrderItem();

            orderItem.setOrderDetailId(detail.getOrderDetailId());
            orderItem.setProductId(detail.getOrderDetailId());
            orderItem.setAmount(detail.getAmount());
            orderItem.setTotal(detail.getTotal());
            orderItem.setProductId(detail.getProduct().getProductId());
            orderItem.setProductName(detail.getProduct().getProductName());
            orderItem.setProductPrice(detail.getProduct().getPrice());

            orderItems.add(orderItem);
        }
        response.setOrderItem(orderItems);
        return response;
    }

    public Orders create(
            String status, String name, String address, String postcode,
            String district, String province, String phoneNumber,
            int total, long customer_id)
    {
        Orders order = new Orders();
        User customer = userRepo.findByUserId(customer_id);
        order.setStatus(status);
        order.setName(name);
        order.setAddress(address);
        order.setPostcode(postcode);
        order.setDistrict(district);
        order.setProvince(province);
        order.setPhoneNumber(phoneNumber);
        order.setTotal(total);
        order.setUser(customer);

        order = save(order);
        return order;
    }
}
