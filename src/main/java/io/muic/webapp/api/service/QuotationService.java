package io.muic.webapp.api.service;

import com.sun.org.apache.xpath.internal.operations.Quo;
import io.muic.webapp.api.entity.Product;
import io.muic.webapp.api.entity.Quotation;
import io.muic.webapp.api.entity.QuotationDetail;
import io.muic.webapp.api.entity.User;
import io.muic.webapp.api.form.QuotationItem;
import io.muic.webapp.api.form.QuotationResponse;
import io.muic.webapp.api.form.TotalStatus;
import io.muic.webapp.api.repository.QuotationDetailRepository;
import io.muic.webapp.api.repository.QuotationRepository;
import io.muic.webapp.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bank on 11/12/17.
 */
@Service
public class QuotationService {
    @Autowired
    private QuotationRepository quoteRepo;
    @Autowired
    private QuotationDetailRepository quoteDetailRepo;
    @Autowired
    private UserRepository userRepo;


    public Quotation save(Quotation quotation) {
        return quoteRepo.save(quotation);
    }

    public List<Quotation> getAllQuotation() {
        return quoteRepo.findAll();
    }

    private Quotation findById(long id){ return quoteRepo.findByQuotationId(id);}

    public TotalStatus getQuotationTotalStatus(String option, long id){

        List<Quotation> quotations = new ArrayList<>();
        if (option.equals("USER")){
            User user = userRepo.findByUserId(id);
            quotations = quoteRepo.findByUser(user);

        } else if (option.equals("ADMIN")){
            quotations= getAllQuotation();
        }

        TotalStatus response = new TotalStatus();
        int total = 0; int statA = 0; int statB = 0; int statC = 0; int statD = 0;
        for (Quotation quote:quotations){
            String status = quote.getStatus();
            total += 1;
            switch (status) {
                case "Requested":
                    statA += 1;
                    break;
                case "Offered":
                    statB += 1;
                    break;
                case "Approved":
                    statC += 1;
                    break;
                case "Disapproved":
                    statD += 1;
                    break;
            }
        }
        response.setTotal(total);
        response.setStatA(statA);
        response.setStatB(statB);
        response.setStatC(statC);
        response.setStatD(statD);
        return response;
    }

    public QuotationResponse updateStatusById(long id, String status){
        Quotation quote = quoteRepo.findByQuotationId(id);
        quote.setStatus(status);
        save(quote);
        return customResponseById(id);
    }

    public List<Quotation> findByCustomerId(long id) {
        User user = userRepo.findByUserId(id);
        return quoteRepo.findByUser(user);
    }

    public QuotationResponse customResponseById(long id){
        Quotation quote = quoteRepo.findByQuotationId(id);
        QuotationResponse response = new QuotationResponse();
        response.setQuotation(quote);
        List<QuotationDetail> quotationDetails = quoteDetailRepo.findByQuotation(quote);
        List<QuotationItem> quoteItems = new ArrayList<>();

        for (QuotationDetail detail: quotationDetails){
            Product product = detail.getProduct();
            QuotationItem quoteItem = new QuotationItem();

            quoteItem.setQuotationDetailId(detail.getOrderDetailId());
            quoteItem.setpId(product.getProductId());
            quoteItem.setProductName(product.getProductName());
            quoteItem.setProductPrice(product.getPrice());
            quoteItem.setProposedQuantity(detail.getProposedQuantity());
            quoteItem.setNormalPrice(product.getPrice()*detail.getProposedQuantity());
            quoteItem.setOfferedPrice(detail.getOfferPrice());

            quoteItems.add(quoteItem);
        }
        response.setQuotationItemList(quoteItems);
        return response;
    }

    public Quotation create(
            String companyName, String companyBranch,
            String address, String postcode,
            String district, String province,
            String taxId, String contactName,
            String contactTel, String contactEmail,
            String remark, String status,
            long uId, int totalItem, int totalQuantity)
    {
        User user = userRepo.findByUserId(uId);
        Quotation quote = new Quotation();
        quote.setCompanyName(companyName);
        quote.setCompanyBranch(companyBranch);
        quote.setAddress(address);
        quote.setPostcode(postcode);
        quote.setDistrict(district);
        quote.setProvince(province);
        quote.setTaxId(taxId);
        quote.setContactName(contactName);
        quote.setContactTel(contactTel);
        quote.setContactEmail(contactEmail);
        quote.setRemark(remark);
        quote.setStatus(status);
        quote.setUser(user);
        quote.setTotalItem(totalItem);
        quote.setTotalQuantity(totalQuantity);

        quote = save(quote);
        return quote;
    }
}
