package io.muic.webapp.api.service;

import io.muic.webapp.api.entity.Product;
import io.muic.webapp.api.form.ProductItem;
import io.muic.webapp.api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bank on 10/31/17.
 */
@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepo;

    public Product findById(long id) {
        return productRepo.findByProductId(id);
    }

    public List<Product> getAllProduct() {
        return productRepo.findAll();
    }

    public Product save(Product product) {
        return productRepo.save(product);
    }

    public Product create(
            String productName, String productDescription, int price, int quantity
            , double length, double width, double height, double weight, byte[] image)
    {
        Product product = new Product();
        product.setProductName(productName);
        product.setProductDescription(productDescription);
        product.setPrice(price);
        product.setQuantity(quantity);
        product.setLength(length);
        product.setWidth(width);
        product.setHeight(height);
        product.setWeight(weight);
        product.setImage(image);

        product = save(product);
        return product;
    }

    public List<ProductItem> getAllProductSelectList(){
        List<Product> allProduct = getAllProduct();
        List<ProductItem> response = new ArrayList<>();

        for(Product product: allProduct){
            ProductItem productItem = new ProductItem();
            productItem.setpId(product.getProductId());
            productItem.setName(product.getProductName());

            response.add(productItem);
        }
        return response;
    }

 }
