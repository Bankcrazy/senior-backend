package io.muic.webapp.api.repository;

import io.muic.webapp.api.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bank on 10/30/17.
 */
public interface AdminRepository extends JpaRepository<Admin,Long> {
    Admin save(Admin admin);

    Admin findByUsername(String username);
}
