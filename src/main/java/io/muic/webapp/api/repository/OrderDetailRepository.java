package io.muic.webapp.api.repository;


import io.muic.webapp.api.entity.OrderDetail;
import io.muic.webapp.api.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Bank on 11/9/17.
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail,Long> {
    OrderDetail save(OrderDetail orderDetail);

    List<OrderDetail> findByOrders(Orders order);
}
