package io.muic.webapp.api.repository;

import io.muic.webapp.api.entity.Quotation;
import io.muic.webapp.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Bank on 11/12/17.
 */
public interface QuotationRepository extends JpaRepository<Quotation,Long> {
    Quotation save(Quotation quotation);

    Quotation findByQuotationId(long quotationId);

    List<Quotation> findByUser(User user);

}
