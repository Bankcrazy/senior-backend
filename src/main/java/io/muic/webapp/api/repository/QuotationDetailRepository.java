package io.muic.webapp.api.repository;

import io.muic.webapp.api.entity.Quotation;
import io.muic.webapp.api.entity.QuotationDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Bank on 11/20/17.
 */
public interface QuotationDetailRepository extends JpaRepository<QuotationDetail,Long> {
    QuotationDetail save(QuotationDetail quoteDetail);

    List<QuotationDetail> findByQuotation(Quotation quotation);
}
