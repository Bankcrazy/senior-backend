package io.muic.webapp.api.repository;

import io.muic.webapp.api.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bank on 10/30/17.
 */
public interface ProductRepository extends JpaRepository<Product,Long> {
    Product save(Product product);

    Product findByProductId(long productId);

}
