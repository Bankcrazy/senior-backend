package io.muic.webapp.api.repository;

import io.muic.webapp.api.entity.Orders;
import io.muic.webapp.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Bank on 10/30/17.
 */
public interface OrderRepository extends JpaRepository<Orders,Long> {
    Orders save(Orders order);

    Orders findByOrderId(long orderId);

    List<Orders> findByUser(User user);
}
