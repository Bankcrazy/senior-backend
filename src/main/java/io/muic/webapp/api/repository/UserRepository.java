package io.muic.webapp.api.repository;

import io.muic.webapp.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bank on 10/18/17.
 */
public interface UserRepository extends JpaRepository<User,Long> {
    User save(User user);

    User findByEmail(String email);

    User findByUserId(long id);
}
