package io.muic.webapp.api.entity;

import javax.persistence.*;

/**
 * Created by Bank on 11/7/17.
 */
@Entity
@Table(name = "Order_Details")
public class OrderDetail {
    @Id
    @GeneratedValue
    private Long orderDetailId;
    // how many product
    private int amount;
    // price of a product * amount
    private int total;

    @ManyToOne
    @JoinColumn(name = "Orders")
    private Orders orders;

    @ManyToOne
    @JoinColumn(name = "Product")
    private Product product;

    public Long getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(Long orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
