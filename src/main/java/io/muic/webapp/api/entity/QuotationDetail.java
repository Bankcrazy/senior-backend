package io.muic.webapp.api.entity;

import javax.persistence.*;

/**
 * Created by Bank on 11/20/17.
 */
@Entity
@Table(name = "Quotation_Details")
public class QuotationDetail {
    @Id
    @GeneratedValue
    private Long orderDetailId;

    private int proposedQuantity;
    private int offerPrice;

    @ManyToOne
    @JoinColumn(name = "Product")
    private Product product;
    @ManyToOne
    @JoinColumn(name = "Quotation")
    private Quotation quotation;

    public Long getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(Long orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public int getProposedQuantity() {
        return proposedQuantity;
    }

    public void setProposedQuantity(int proposedQuantity) {
        this.proposedQuantity = proposedQuantity;
    }

    public int getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(int offerPrice) {
        this.offerPrice = offerPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Quotation getQuotation() {
        return quotation;
    }

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }
}
