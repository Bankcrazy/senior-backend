package io.muic.webapp.api.form;

/**
 * Created by Bank on 11/10/17.
 */
public class Credential {
    private long id;
    private String user;
    private String jwt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
