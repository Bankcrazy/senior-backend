package io.muic.webapp.api.form;

import io.muic.webapp.api.entity.Orders;
import io.muic.webapp.api.entity.User;

import java.util.Date;
import java.util.List;

/**
 * Created by Bank on 11/10/17.
 */
public class OrderResponse {
    private Orders order;
    private List<OrderItem> orderItem;

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    public List<OrderItem> getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(List<OrderItem> orderItem) {
        this.orderItem = orderItem;
    }
}
