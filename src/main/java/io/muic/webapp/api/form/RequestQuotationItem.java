package io.muic.webapp.api.form;

/**
 * Created by Bank on 11/20/17.
 */
public class RequestQuotationItem {
    private long pId;
    private int quantity;

    public long getpId() {
        return pId;
    }

    public void setpId(long pId) {
        this.pId = pId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
