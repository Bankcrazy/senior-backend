package io.muic.webapp.api.form;

import java.util.List;

/**
 * Created by Bank on 11/22/17.
 */
public class OfferPrice {
    private long quotationId;
    private List<QuotationItem> items;

    public long getQuotationId() {
        return quotationId;
    }

    public void setQuotationId(long quotationId) {
        this.quotationId = quotationId;
    }

    public List<QuotationItem> getItems() {
        return items;
    }

    public void setItems(List<QuotationItem> items) {
        this.items = items;
    }
}
