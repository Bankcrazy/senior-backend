package io.muic.webapp.api.form;

/**
 * Created by Bank on 11/21/17.
 */
public class QuotationItem {
    private long QuotationDetailId;
    private long pId;
    private String productName;
    private int productPrice;
    private int proposedQuantity;
    private int normalPrice;
    private int offeredPrice;

    public long getQuotationDetailId() {
        return QuotationDetailId;
    }

    public void setQuotationDetailId(long quotationDetailId) {
        QuotationDetailId = quotationDetailId;
    }

    public long getpId() {
        return pId;
    }

    public void setpId(long pId) {
        this.pId = pId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public int getProposedQuantity() {
        return proposedQuantity;
    }

    public void setProposedQuantity(int proposedQuantity) {
        this.proposedQuantity = proposedQuantity;
    }

    public int getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(int normalPrice) {
        this.normalPrice = normalPrice;
    }

    public int getOfferedPrice() {
        return offeredPrice;
    }

    public void setOfferedPrice(int offeredPrice) {
        this.offeredPrice = offeredPrice;
    }
}
