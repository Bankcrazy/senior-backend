package io.muic.webapp.api.form;

/**
 * Created by Bank on 11/9/17.
 */
public class CheckoutItem {
    private long productId;
    private int amount;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
