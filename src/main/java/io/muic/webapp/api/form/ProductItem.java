package io.muic.webapp.api.form;

/**
 * Created by Bank on 11/20/17.
 */
public class ProductItem {
    private long pId;
    private String name;

    public long getpId() {
        return pId;
    }

    public void setpId(long pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
