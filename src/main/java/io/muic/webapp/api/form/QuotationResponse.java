package io.muic.webapp.api.form;

import io.muic.webapp.api.entity.Quotation;

import java.util.Date;
import java.util.List;

/**
 * Created by Bank on 11/12/17.
 */
public class QuotationResponse {
    private Quotation quotation;
    private List<QuotationItem> quotationItemList;

    public Quotation getQuotation() {
        return quotation;
    }

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    public List<QuotationItem> getQuotationItemList() {
        return quotationItemList;
    }

    public void setQuotationItemList(List<QuotationItem> quotationItemList) {
        this.quotationItemList = quotationItemList;
    }
}
