package io.muic.webapp.api.form;

/**
 * Created by Bank on 11/12/17.
 */
public class TotalStatus {
    // shared status
    private int total;
    private int placed;
//   status legend:
//          Order/Quotation
//    A    Placed/Requested
//    B    In-Progress/Offered
//    C    Shipped/Approved
//    D    Delivered/Disapproved

    private int statA;
    private int statB;
    private int statC;
    private int statD;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPlaced() {
        return placed;
    }

    public void setPlaced(int placed) {
        this.placed = placed;
    }

    public int getStatA() {
        return statA;
    }

    public void setStatA(int statA) {
        this.statA = statA;
    }

    public int getStatB() {
        return statB;
    }

    public void setStatB(int statB) {
        this.statB = statB;
    }

    public int getStatC() {
        return statC;
    }

    public void setStatC(int statC) {
        this.statC = statC;
    }

    public int getStatD() {
        return statD;
    }

    public void setStatD(int statD) {
        this.statD = statD;
    }
}
